#ifndef LEAP_GODOT_H
#define LEAP_GODOT_H

#include <iostream>
#include <cstring>

#include <Leap.h>

#include <Godot.hpp>
#include <NativeScript.hpp>

using namespace Leap;

namespace godot {

        enum State {
                    CONNECTED, DISCONNECTED, EXITED, LIVE
        };

        class leap_godot : public GodotScript<NativeScript>, public Listener {
                GODOT_CLASS(leap_godot)

        public:
                static void _register_methods();

                leap_godot();
                ~leap_godot();

                void _process(float delta);

                float get_pitch();
                float get_roll();
                float get_yaw();

                // leap listener methods
                virtual void onInit(const Controller&);
                virtual void onConnect(const Controller&);
                virtual void onDisconnect(const Controller&);
                virtual void onExit(const Controller&);
                virtual void onFrame(const Controller&);
                virtual void onFocusGained(const Controller&);
                virtual void onFocusLost(const Controller&);
                virtual void onDeviceChange(const Controller&);
                virtual void onServiceConnect(const Controller&);
                virtual void onServiceDisconnect(const Controller&);

        private:
                Controller controller;

                State state;

                float hand_pitch;
                float hand_roll;
                float hand_yaw;
        };

}

#endif
