#include "leap_godot.h"

using namespace godot;

void leap_godot::_register_methods() {
        register_method((char *)"_process", &leap_godot::_process);
        register_method((char *)"get_pitch", &leap_godot::get_pitch);
        register_method((char *)"get_roll", &leap_godot::get_roll);
        register_method((char *)"get_yaw", &leap_godot::get_yaw);
}

leap_godot::leap_godot() {
        // Have the sample listener receive events from the controller
        controller.addListener(*this);

        // if (argc > 1 && strcmp(argv[1], "--bg") == 0)
        //         controller.setPolicy(Leap::Controller::POLICY_BACKGROUND_FRAMES);

}

leap_godot::~leap_godot() {
        // Remove use from the leap controller
        controller.removeListener(*this);
}

void leap_godot::_process(float delta) {
}

float leap_godot::get_pitch() {
        return hand_pitch;
}

float leap_godot::get_roll() {
        return hand_roll;
}

float leap_godot::get_yaw() {
        return hand_yaw;
}

void leap_godot::onInit(const Controller& controler) {
        std::cout << "Init" << std::endl;
}

void leap_godot::onConnect(const Controller& controller) {
  std::cout << "Connected" << std::endl;
}

void leap_godot::onDisconnect(const Controller& controller) {
  // Note: not dispatched when running in a debugger.
  std::cout << "Disconnected" << std::endl;
}

void leap_godot::onExit(const Controller& controller) {
  std::cout << "Exited" << std::endl;
}

void leap_godot::onFrame(const Controller& controller) {
  // Get the most recent frame and report some basic information
  const Frame frame = controller.frame();
  std::cout << "Frame id: " << frame.id()
            << ", timestamp: " << frame.timestamp()
            << ", hands: " << frame.hands().count()
            << ", extended fingers: " << frame.fingers().extended().count()
            << ", tools: " << frame.tools().count()
            << ", gestures: " << frame.gestures().count() << std::endl;

  HandList hands = frame.hands();
  for (HandList::const_iterator hl = hands.begin(); hl != hands.end(); ++hl) {
    // Get the first hand
    const Hand hand = *hl;
    std::string handType = hand.isLeft() ? "Left hand" : "Right hand";
    std::cout << std::string(2, ' ') << handType << ", id: " << hand.id()
              << ", palm position: " << hand.palmPosition() << std::endl;
    // Get the hand's normal vector and direction
    const Vector normal = hand.palmNormal();
    const Vector direction = hand.direction();

    // Calculate the hand's pitch, roll, and yaw angles
    std::cout << std::string(2, ' ') <<  "pitch: " << direction.pitch() * RAD_TO_DEG << " degrees, "
              << "roll: " << normal.roll() * RAD_TO_DEG << " degrees, "
              << "yaw: " << direction.yaw() * RAD_TO_DEG << " degrees" << std::endl;

    hand_pitch = direction.pitch() * RAD_TO_DEG;
    hand_roll =  normal.roll() * RAD_TO_DEG;
    hand_yaw = direction.yaw() * RAD_TO_DEG;

    // Get the Arm bone
    Arm arm = hand.arm();
    std::cout << std::string(2, ' ') <<  "Arm direction: " << arm.direction()
              << " wrist position: " << arm.wristPosition()
              << " elbow position: " << arm.elbowPosition() << std::endl;
  }

  if (!frame.hands().isEmpty()) {
    std::cout << std::endl;
  }

}

void leap_godot::onFocusGained(const Controller& controller) {
  std::cout << "Focus Gained" << std::endl;
}

void leap_godot::onFocusLost(const Controller& controller) {
  std::cout << "Focus Lost" << std::endl;
}

void leap_godot::onDeviceChange(const Controller& controller) {
  std::cout << "Device Changed" << std::endl;
  const DeviceList devices = controller.devices();

  for (int i = 0; i < devices.count(); ++i) {
    std::cout << "id: " << devices[i].toString() << std::endl;
    std::cout << "  isStreaming: " << (devices[i].isStreaming() ? "true" : "false") << std::endl;
  }
}

void leap_godot::onServiceConnect(const Controller& controller) {
  std::cout << "Service Connected" << std::endl;
}

void leap_godot::onServiceDisconnect(const Controller& controller) {
        std::cout << "Service Disconnected" << std::endl;
}
