#!python
import os

# define our target
final_lib_path = 'bin/'
lib_abbrev = ''
target_path = '/addons/leap_godot/bin/'
target_name = 'libleap_godot'

# Local dependency paths, adapt them to your setup
godot_headers_path = "godot-cpp/godot_headers/"
cpp_bindings_path = "godot-cpp/"
cpp_library = "godot-cpp"
leapsdk_path = ARGUMENTS.get("leapsdk_path", "leapsdk")

target = ARGUMENTS.get("target", "debug")
platform = ARGUMENTS.get("platform", "windows")
bits = ARGUMENTS.get("bits", 64)

# This makes sure to keep the session environment variables on windows, 
# that way you can run scons in a vs 2017 prompt and it will find all the required tools
env = Environment()
if platform == "windows":
    env = Environment(ENV = os.environ)

if ARGUMENTS.get("use_llvm", "no") == "yes":
    env["CXX"] = "clang++"

def add_sources(sources, directory):
    for file in os.listdir(directory):
        if file.endswith('.cpp'):
            sources.append(directory + '/' + file)

if platform == "osx":
    env.Append(CCFLAGS = ['-g','-O3', '-arch', 'x86_64', '-std=c++14'])
    env.Append(LINKFLAGS = ['-arch', 'x86_64', '-framework', 'Cocoa', '-Wl,-undefined,dynamic_lookup'])
    lib_abbrev = "osx"

    final_lib_path = final_lib_path + 'osx/'

elif platform == "linux":
    env.Append(CCFLAGS = ['-fPIC', '-g','-std=c++14'])
    env.Append(LINKFLAGS = ['-Wl,-R,\'$$ORIGIN\''])
    lib_abbrev = "x11"

    final_lib_path = final_lib_path + 'x11/'

elif platform == "windows":
    if target == "debug":
        env.Append(CCFLAGS = ['-EHsc', '-D_DEBUG', '-MDd'])
    else:
        env.Append(CCFLAGS = ['-O2', '-EHsc', '-DNDEBUG', '-MD'])
    env.Append(LINKFLAGS = ['/WX'])
    lib_abbrev = "win" + str(bits)
    final_lib_path = final_lib_path + 'win' + str(bits) + '/'

env.Append(CPPPATH=['src/', leapsdk_path + '/include/', "godot-cpp/godot_headers/", 'godot-cpp/include/', 'godot-cpp/include/core/'])
env.Append(LDFLAGS = ['-Xlinker --unresolved-symbols=ignore-in-shared-libs'])
env.Append(LIBPATH=["godot-cpp/bin", "bin/" + lib_abbrev])
env.Append(LIBS=["godot-cpp" + "." + platform + "." + str(bits), "Leap"])

sources = []
add_sources(sources, "src")

library = env.SharedLibrary(target=final_lib_path + target_name, source=sources)
Default(library)
