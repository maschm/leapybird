#ifndef TERRAIN_GENERATION_H
#define TERRAIN_GENERATION_H

#include <Godot.hpp>
#include <NativeScript.hpp>
#include <Material.hpp>
#include <MeshInstance.hpp>
#include <SurfaceTool.hpp>
#include "PerlinNoise.h"

namespace godot
{

class terrain_generation : public godot::GodotScript<NativeScript>
{
        GODOT_CLASS(terrain_generation)

      public:
        static void _register_methods();

        terrain_generation();
        ~terrain_generation();

        MeshInstance generate(const Vector3 &start, const Vector3 &end);

      private:

        SurfaceTool gen_grid(const Vector3 &start, const Vector3 &end, unsigned int res);
        void get_noise(Vector3 &pos);
        double map_coord(double x);

        Material material;
        PerlinNoise perlin;
};

} // namespace godot

#endif
