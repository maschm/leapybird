#include <numeric>
#include <random>
#include <algorithm>
#include <math.h>

#include <PerlinNoise.h>

// TODO: Acknowledge the fact that I just blatantly copied all this from
// [1] https://cs.nyu.edu/~perlin/noise/
// [2] https://solarianprogrammer.com/2012/07/18/perlin-noise-cpp-11/

PerlinNoise::PerlinNoise() {
     
}

// Generate a new permutation vector based on the value of seed
PerlinNoise::PerlinNoise(unsigned int seed)
{
    p.resize(256);

    // Fill p with values from 0 to 255
    std::iota(p.begin(), p.end(), 0);

    // Initialize a random engine with seed
    std::default_random_engine engine(seed);

    // Suffle  using the above random engine
    std::shuffle(p.begin(), p.end(), engine);

    // Duplicate the permutation vector
    p.insert(p.end(), p.begin(), p.end());
}

double PerlinNoise::noise(double x, double y, double z)
{
    int     X = (int) std::floor(x) & 255,
            Y = (int) std::floor(y) & 255,
            Z = (int) std::floor(z) & 255;

            x -= std::floor(x);
            y -= std::floor(y);
            z -= std::floor(z);

    double  u = fade(x),
            v = fade(y),
            w = fade(z);

    int     A = p[X  ]+Y, AA = p[A]+Z, AB = p[A+1]+Z,   // HASH COORDINATES OF
            B = p[X+1]+Y, BA = p[B]+Z, BB = p[B+1]+Z;   // THE 8 CUBE CORNERS,

    return  lerp(w, lerp(v, lerp(u, grad(p[AA  ], x  , y  , z   ),  // AND ADD
                                    grad(p[BA  ], x-1, y  , z   )), // BLENDED
                            lerp(u, grad(p[AB  ], x  , y-1, z   ),  // RESULTS
                                    grad(p[BB  ], x-1, y-1, z   ))),// FROM  8
                    lerp(v, lerp(u, grad(p[AA+1], x  , y  , z-1 ),  // CORNERS
                                    grad(p[BA+1], x-1, y  , z-1 )), // OF CUBE
                            lerp(u, grad(p[AB+1], x  , y-1, z-1 ),
                                    grad(p[BB+1], x-1, y-1, z-1 ))));
}
