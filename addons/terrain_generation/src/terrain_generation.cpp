#include <Mesh.hpp>
#include <ArrayMesh.hpp>

#include "terrain_generation.h"

using namespace godot;

void terrain_generation::_register_methods()
{
        register_method((char *)"generate", &terrain_generation::generate);
        // register_property<terrain_generation, Material>((char *)"material", &terrain_generation::material, Material());
}

terrain_generation::terrain_generation()
{
        unsigned int seed = 0;
        perlin = PerlinNoise(seed);
}

terrain_generation::~terrain_generation()
{
}

SurfaceTool terrain_generation::gen_grid(const Vector3 &start, const Vector3 &end, unsigned int res)
{
        SurfaceTool st = SurfaceTool();
        st.begin(Mesh::PRIMITIVE_TRIANGLES);
        double uv_scale = 0.1;
        Vector3 n;

        for (int x = start[0]; x < end[0]; x += res)
        {
                for (int z = start[2]; z < end[2]; z += res)
                {
                        st.add_uv(Vector2(x, z) * uv_scale);
                        n = Vector3(x, 0, z);
                        get_noise(n);
                        st.add_vertex(n);

                        st.add_uv(Vector2(x + res, z) * uv_scale);
                        n = Vector3(x + res, 0, z);
                        get_noise(n);
                        st.add_vertex(n);

                        st.add_uv(Vector2(x, z + res) * uv_scale);
                        n = Vector3(x, 0, z + res);
                        get_noise(n);
                        st.add_vertex(n);

                        st.add_uv(Vector2(x + res, z) * uv_scale);
                        n = Vector3(x + res, 0, z);
                        get_noise(n);
                        st.add_vertex(n);

                        st.add_uv(Vector2(x + res, z + res) * uv_scale);
                        n = Vector3(x + res, 0, z + res);
                        get_noise(n);
                        st.add_vertex(n);

                        st.add_uv(Vector2(x, z + res) * uv_scale);
                        n = Vector3(x, 0, z + res);
                        get_noise(n);
                        st.add_vertex(n);
                }
        }
        return st;
}

MeshInstance terrain_generation::generate(const Vector3 &start, const Vector3 &end)
{
        SurfaceTool st = gen_grid(start, end, 1);
        st.generate_normals();
        st.generate_tangents();
        // st.set_material(&material);
        Ref<Mesh> mesh = st.commit();

        MeshInstance meshInst = MeshInstance();
        meshInst.set_mesh(mesh);
        meshInst.create_trimesh_collision();

        return meshInst;
}

// CURRENT USAGE: noise(x, 0, z).
// COULD ALSO BE: noise(x, z, 0)
void terrain_generation::get_noise(Vector3 &pos)
{
        double long_freq = .005;
        double short_freq = .025;
        double long_height = 100;
        double short_height = 20;

        pos[1] = long_height * perlin.noise(map_coord(pos[0]) * long_freq, 0, map_coord(pos[2]) * long_freq);
        pos[1] += short_height * perlin.noise(map_coord(pos[0]) * short_freq, 0, map_coord(pos[2]) * short_freq);
}

double terrain_generation::map_coord(double x)
{
        return x; // TODO
}