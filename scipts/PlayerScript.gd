extends RigidBody

var steer_strength = 1;
var boost_power = 25;
var break_power = 15;

var t = 0;
var wiggle_angle = 18;

var max_distance = 0
signal new_max_distance (dist)

var last_velocity = Vector3(0,0,0)
var has_boost = false

onready var model = $Model

func _integrate_forces(state):
    var steer_yaw = 0
    var steer_pitch = 0
    var steer_roll = 0
    
    if Input.is_action_pressed("steer_left"):
        steer_yaw = 1
    if Input.is_action_pressed("steer_right"):
        steer_yaw = -1
    if Input.is_action_pressed("steer_up"):
        steer_pitch = 1
    if Input.is_action_pressed("steer_down"):
        steer_pitch = -1
        
    if steer_pitch > 0.1 or steer_pitch < -0.1 or steer_yaw > 0.1 or steer_yaw < -0.1:
        #var rot = Quat(0,0,1,steer_roll * state.step) * Quat(1,0,0,steer_pitch * state.step) * Quat(0,1,0,steer_roll * state.step)
        #state.linear_velocity = rot.xform(state.linear_velocity)
        state.linear_velocity = state.linear_velocity.rotated(Vector3(0,1,0), steer_strength * steer_yaw * state.step)
        var pitch_axis = state.linear_velocity.cross(Vector3(0,1,0)).normalized()
        state.linear_velocity = state.linear_velocity.rotated(pitch_axis, steer_strength * steer_pitch * state.step)
    
    if Input.is_action_just_pressed("boost"):
        state.apply_impulse(Vector3(0,0,0), state.linear_velocity.normalized() * boost_power)    
    
    if Input.is_action_pressed("break"):
        apply_impulse(Vector3(0,0,0), state.linear_velocity.normalized() * state.step * -break_power)    

    print(state.linear_velocity.length())
    
    has_boost = false and contacts_reported > 0 and state.linear_velocity.length() - last_velocity.length() > 0.1
    #speed boost if is accelerating
    if has_boost:
        state.linear_velocity += state.linear_velocity * state.step
    last_velocity = state.linear_velocity
    
func _process(delta):
    # move so that the geometry is always facing in the direction of travel
    model.look_at(transform.origin + linear_velocity.normalized(), Vector3(0,1,0))
    
    # notify on new max distance
    if -translation.z > max_distance:
        max_distance = -translation.z
        emit_signal("new_max_distance", max_distance)
    
    t += delta;
    var wiggel = Vector3(wiggle_angle*sin(10*t), wiggle_angle*sin(10*t+PI), wiggle_angle*sin(10*t+PI/2));
    #body.rotation_degrees += wiggel;
