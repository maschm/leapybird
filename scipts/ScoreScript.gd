extends Label

onready var player = get_tree().get_root().find_node("Player", true, false)

func _ready():
    player.connect("new_max_distance", self, "change_score")
    pass
    
func change_score(score):
    set_text("Score: " + str(score))