extends Spatial

export(NodePath) var targetPath;
export(Vector3) var absCamOffset = Vector3(0,5,0);
export(float) var playerToCamDst = 10;

onready var target = get_node(targetPath);

func _process(delta):
    
    var vel = target.linear_velocity;
    
    var pos = target.transform.origin;
    look_at(pos, Vector3(0,1,0));
    
    var camPos = pos+absCamOffset-vel.normalized()*playerToCamDst;

    translation += (camPos-self.translation)*delta;