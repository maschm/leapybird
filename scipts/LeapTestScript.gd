extends Node

var leap = load("res://addons/leap_motion/leap_godot.gdns").new()

func _process(delta):
    print("pitch: " + str(leap.get_pitch()) + " yaw: " + str(leap.get_yaw()) + " roll: " + str(leap.get_roll()))