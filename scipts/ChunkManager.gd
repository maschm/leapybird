extends Spatial

export(NodePath) var generator_path # path to the LandscapeGenerator
export(NodePath) var player_path

onready var generator = get_node(generator_path)
onready var player = get_node(player_path)

var chunk_size = 20

var chunk_active_dist = 10;
var loaded_chunks = {}

func is_chunk_id_loaded(chunk_id):
    return loaded_chunks.has(chunk_id);

func chunk_id_to_bounds(chunk_id):
    return [Vector3(chunk_id.x*chunk_size, 0, chunk_id.y*chunk_size), Vector3((chunk_id.x+1)*chunk_size, 0, (chunk_id.y+1)*chunk_size)]
    
func add_mesh_instance_to_chunk_id(chunk_id, mesh_instance):
    add_child(mesh_instance);
    loaded_chunks[chunk_id] = mesh_instance;
    
func pos_to_chunk_id_vec(pos):
    return Vector2(int(pos.x-chunk_size/2)/int(chunk_size), int(pos.z-chunk_size/2)/int(chunk_size))
    
func chunk_id_to_center_pos(chunk_id):
    return Vector2((chunk_id.x+0.5)*chunk_size, (chunk_id.y+0.5)*chunk_size)

func unload_from_chunk_id(chunk_id):
    #remove_child(loaded_chunks[chunk_id])
    loaded_chunks[chunk_id].queue_free()
    #loaded_chunks[chunk_id].free();
    loaded_chunks.erase(chunk_id)

func _process(delta):
    var player_pos = player.transform.origin
    var player_pos_2d = Vector2(player_pos.x, player_pos.z);
    var chunk_id = pos_to_chunk_id_vec(player_pos)
    
    var start = chunk_id - Vector2(chunk_active_dist, chunk_active_dist);
    var end = chunk_id + Vector2(chunk_active_dist, chunk_active_dist);
        
    var pre = str(loaded_chunks.size())
    # unload chunk 
    for loaded_chunk in loaded_chunks:
        if player_pos_2d.distance_to(chunk_id_to_center_pos(loaded_chunk)) > chunk_active_dist * chunk_size:
            unload_from_chunk_id(loaded_chunk);
    
    for x in range(start.x, end.x, 1):
        for y in range(start.y, end.y, 1):
            var rel_chunk_id = Vector2(x,y);
            if player_pos_2d.distance_to(chunk_id_to_center_pos(rel_chunk_id)) < chunk_active_dist * chunk_size:
                if !is_chunk_id_loaded(rel_chunk_id):
                    var bounds = chunk_id_to_bounds(rel_chunk_id)
                    var inst = generator.generate(bounds[0], bounds[1])
                    add_mesh_instance_to_chunk_id(rel_chunk_id, inst)
    
    print("Chunks " + pre + " " + str(loaded_chunks.size()));