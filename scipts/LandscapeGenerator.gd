extends MeshInstance

export(Material) var material

var terrain = load("res://addons/terrain_generation/terrain_generation.gdns").new()

export var mesh_resolution = 1
export var uv_scale = 0.1

func f(p):
    p.y = 5*cos(p.x/10 + p.z/14) + 5*cos(p.x/16) + 5*cos(p.x/12) + 5*cos(p.z/10);
    return p;
   
    
func p(p):
    return f(p)
    p.y = terrain.get_noise(p.x/200, p.z/200) * 100
    p.y += terrain.get_noise(p.x/40+100, p.z/40+100) * 20
    return p;
    
func gen_grid(start, end, old_mesh):
    if old_mesh == null:
        var st = SurfaceTool.new()
        st.begin(Mesh.PRIMITIVE_TRIANGLES)
        for x in range(start.x, end.x, mesh_resolution):
            for z in range(start.z, end.z, mesh_resolution):
                st.add_uv(Vector2(x, z)*uv_scale)
                st.add_vertex(p(Vector3(x,    0,z)))
                st.add_uv(Vector2(x+mesh_resolution, z)*uv_scale)
                st.add_vertex(p(Vector3(x+mesh_resolution,0,z)))
                st.add_uv(Vector2(x, z+mesh_resolution)*uv_scale)
                st.add_vertex(p(Vector3(x,    0,z+mesh_resolution)))
                st.add_uv(Vector2(x+mesh_resolution, z)*uv_scale)
                st.add_vertex(p(Vector3(x+mesh_resolution,0,z)))
                st.add_uv(Vector2(x+mesh_resolution, z+mesh_resolution)*uv_scale)
                st.add_vertex(p(Vector3(x+mesh_resolution,0,z+mesh_resolution)))
                st.add_uv(Vector2(x, z+mesh_resolution)*uv_scale)
                st.add_vertex(p(Vector3(x,    0,z+mesh_resolution)))
        st.generate_normals()
        st.generate_tangents()
        st.set_material(material)
        return st.commit()
    else:
        var mdt = MeshDataTool.new()
        mdt.create_from_surface(old_mesh, 0)
        var i = 0
        for x in range(start.x, end.x, mesh_resolution):
            for z in range(start.z, end.z, mesh_resolution):
                mdt.set_vertex_uv(i, Vector2(x, z)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x,    0,z)))
                i += 1
                mdt.set_vertex_uv(i, Vector2(x+mesh_resolution, z)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x+mesh_resolution,0,z)))
                i += 1
                mdt.set_vertex_uv(i, Vector2(x, z+mesh_resolution)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x,    0,z+mesh_resolution)))
                i += 1
                mdt.set_vertex_uv(i, Vector2(x+mesh_resolution, z)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x+mesh_resolution,0,z)))
                i += 1
                mdt.set_vertex_uv(i, Vector2(x+mesh_resolution, z+mesh_resolution)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x+mesh_resolution,0,z+mesh_resolution)))
                i += 1
                mdt.set_vertex_uv(i, Vector2(x, z+mesh_resolution)*uv_scale)
                mdt.set_vertex(i, p(Vector3(x,    0,z+mesh_resolution)))
                i += 1
        mdt.commit_to_surface(old_mesh)
        return old_mesh

func generate(start, end, old_mesh = null):
    var new_mesh = gen_grid(start, end, old_mesh)
    
    var meshInst = MeshInstance.new()
    meshInst.mesh = new_mesh;
    meshInst.create_trimesh_collision()
    
    return meshInst